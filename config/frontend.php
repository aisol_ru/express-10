<?php

$config = [
    'modules' => [
        'ajax' => [
            'class' => 'app\modules\ajax\Module',
        ],
    ],
    'components' => [
        'user' => [
            'loginUrl' => ['account/login'],
        ],
        'urlManager' => [
            'class' => 'app\components\UrlManager',
            'rules' => [
                ['class' => 'app\components\url_rules\DefaultUrlRule'],
            ]
        ],
        'view' => [
            'class' => 'app\components\View',
            
            'minifyOutput' => true,
            'minifyPath' => '@webroot/assets/minify',
            'forceCharset' => 'UTF-8',
            'compressOptions' => ['extra' => true, 'no-comments' => true],

        ],
    ],
];

if (YII_ENV === 'dev') {
    $config['components']['view']['enableMinify'] = false;
    $config['components']['assetManager']['appendTimestamp'] = true;
}

return $config;
