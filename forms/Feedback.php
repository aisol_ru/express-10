<?php

namespace app\forms;

use yii\base\Model;

class Feedback extends Model
{

    public $name;
    public $content;

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Обязательно поле'],
            ['content', 'required', 'message' => 'Обязательно поле'],
        ];
    }

}
