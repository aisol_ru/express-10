<?php

namespace app\forms;

use app\models\user\User;
use yii\base\Model;
use Yii;

class Consultation extends Model
{

    public $name;
    public $phone;
    public $message;
    public $place;

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Обязательно поле'],
            ['phone', 'required', 'message' => 'Обязательно поле'],
            ['message', 'filter', 'filter' => function($value) {
                return nl2br(trim(strip_tags($value)));
            }],
            ['message', 'required', 'message' => 'Обязательно поле'],
        ];
    }

}
