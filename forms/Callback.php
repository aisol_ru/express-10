<?php

namespace app\forms;

use app\models\user\User;
use yii\base\Model;
use Yii;

class Callback extends Model
{

    public $name;
    public $phone;

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Обязательно поле'],
            ['phone', 'required', 'message' => 'Обязательно поле'],
        ];
    }

}
