<?php

namespace app\forms;

use Yii;
use yii\base\Model;
use app\models\User;

class LoginForm extends Model
{

    public $num_cart;
    public $pin;
    public $rememberMe = true;
    private $_user = false;

    public function rules()
    {
        return [
            ['num_cart', 'required', 'message' => 'Заполните номер карты'],
            ['pin', 'required', 'message' => 'Заполните пин код'],
            ['rememberMe', 'boolean'],
            ['pin', 'validatePassword'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->pin)) {
                $this->addError($attribute, 'Доступ закрыт');
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByLogin($this->num_cart);
        }
        return $this->_user;
    }

}
