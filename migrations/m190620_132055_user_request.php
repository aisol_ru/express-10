<?php

use yii\db\Migration;

class m190620_132055_user_request extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%user_request}}', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(1)->unsigned()->defaultValue(0)->notNull()->comment('Тип'),
            'is_sended' => $this->integer(1)->unsigned()->defaultValue(0)->notNull()->comment('Флаг отправки email\'а'),
            'json' => $this->text()->comment('Содержимое формы'),

            'created_at' => $this->integer()->unsigned()->comment('Дата создания'),
            'updated_at' => $this->integer()->unsigned()->comment('Дата изменения'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%user_request}}');
    }

}
