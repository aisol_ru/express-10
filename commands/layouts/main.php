<?php
/* @var $this app\components\View */

use yii\helpers\Html;
use app\assets\AppAsset;
use app\widgets\coolbaby\BasketMini;
use yii\widgets\ActiveForm;
use app\forms\SearchForm;
use app\models\Parameter;

AppAsset::register($this);
?>



<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-153608750-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-153608750-1');
        </script>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="yandex-verification" content="dd30a1fd5ca39a94"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

         
    <body>

        <?php $this->beginBody() ?>

         <div class="fade-wrap"></div>

    <div class="site-wrapper">
        <header  class="header">

            <div class="header-top-row-wrap">
                <div class="header-top-row page-wrapper">
                    <?= app\widgets\coolbaby\BottomMenu::widget() ?>
                    
                </div>
            </div>

            <div class="header-bot-row-wrap">
                <div class="header-bot-row page-wrapper">
                    <div class="logo-block">
                        <a href="/">
                            <img src="<?= Parameter::getValue(2, true, true) ?>" alt="<?= Yii::$app->name ?>">
                        </a>
                    </div>
                    <div class="header-cont-block">
                        <?php if (empty(Parameter::getValue(4, false, true)) === false) { ?>
                            <a href="mailto:<?= Parameter::getValue(4, false, true) ?>">
                                <div class="hcl-pic-wrap"><img src="<?= AppAsset::path('img/icon-mail.svg') ?>" alt="">
                                </div><?= Parameter::getValue(4, false, true) ?></a>
                        <?php } ?>
                        <?php if (empty(Parameter::getValue(5, false, true)) === false) { ?>
                            <a href="tel:+<?= preg_replace('/[\D]/', '', Parameter::getValue(5, false, true)) ?>">
                                <div class="hcl-pic-wrap">
                                    <img src="<?= AppAsset::path('img/icon-phone.svg') ?>" alt="">
                                </div><?= Parameter::getValue(5, false, true) ?></a>
                        <?php } ?>
                        <div class="burger-btn js-burger-btn">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>

        </header>
        <div class="page-content">
            
            <?= $content ?>
            
        </div>
        <footer class="footer">
            <div class="footer-wrapper page-wrapper">
                <a href="/" class="footer-logo-link">
                    <img src="<?= Parameter::getValue(3, true, true) ?>" alt="">
                </a>
                <?= Parameter::getValue(6, false, true) ?>
                <div class="mainpage-social">
            <div class="socnets__list">
                <div class="socnets__item socnets__item_vk">
                    <a target="_blank" class="socnets__icon" rel="nofollow" href="https://vk.com/club174787421">
                       <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="640" height="640" viewBox="0 0 640 640">
                            <g id="icomoon-ignore">
                            </g>
                            <path d="M569.664 393.536c0 0 51.744 51.104 64.544 74.752 0.352 0.512 0.512 0.896 0.576 1.12 5.216 8.736 6.496 15.584 3.936 20.64-4.32 8.352-18.944 12.544-23.904 12.896 0 0-88.896 0-91.456 0-6.368 0-19.616-1.664-35.744-12.8-12.32-8.608-24.576-22.784-36.448-36.64-17.728-20.576-33.056-38.432-48.576-38.432-1.984 0-3.904 0.32-5.76 0.96-11.744 3.712-26.656 20.448-26.656 65.024 0 13.952-11.008 21.888-18.72 21.888 0 0-39.936 0-41.888 0-14.272 0-88.576-4.992-154.464-74.464-80.736-85.056-153.248-255.68-153.952-257.184-4.512-11.040 4.96-17.056 15.2-17.056h92.352c12.384 0 16.416 7.488 19.232 14.208 3.264 7.712 15.36 38.56 35.2 73.216 32.128 56.384 51.872 79.328 67.648 79.328 2.976 0 5.792-0.736 8.448-2.24 20.608-11.328 16.768-84.928 15.808-100.096 0-2.944-0.032-32.864-10.592-47.328-7.552-10.368-20.416-14.4-28.192-15.872 2.080-3.008 6.496-7.616 12.16-10.336 14.112-7.040 39.616-8.064 64.928-8.064h14.048c27.456 0.384 34.56 2.144 44.544 4.672 20.096 4.8 20.48 17.824 18.72 62.176-0.512 12.672-1.056 26.944-1.056 43.744 0 3.584-0.16 7.584-0.16 11.648-0.608 22.752-1.408 48.384 14.656 58.912 2.048 1.28 4.448 1.984 6.944 1.984 5.568 0 22.24 0 67.456-77.6 19.84-34.272 35.2-74.688 36.256-77.728 0.896-1.696 3.584-6.464 6.848-8.384 2.496-1.536 5.824-1.792 7.552-1.792h108.64c11.84 0 19.872 1.792 21.44 6.272 2.624 7.264-0.512 29.44-50.112 96.512-8.352 11.168-15.68 20.832-22.112 29.28-44.96 59.008-44.96 61.984 2.656 106.784z"></path>
                        </svg>
                    </a>
                </div>
                <div class="socnets__item socnets__item_odnkl">
                    <a target="_blank" class="socnets__icon" rel="nofollow" href="https://odnoklassniki.ru/profile/197669483586">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="320" height="448" viewBox="0 0 320 448">
                            <g id="icomoon-ignore">
                            </g>
                            <path d="M160 226.75q-47 0-80.25-33.25t-33.25-80q0-47 33.25-80.25t80.25-33.25 80.25 33.25 33.25 80.25q0 46.75-33.25 80t-80.25 33.25zM160 57.5q-23 0-39.375 16.375t-16.375 39.625q0 23 16.375 39.375t39.375 16.375 39.375-16.375 16.375-39.375q0-23.25-16.375-39.625t-39.375-16.375zM290.75 240.5q3.25 6.75 3.75 12.375t-1.125 10.125-6.625 9.625-10.625 9.25-15.375 10.375q-28.75 18.25-78.75 23.5l18.25 18 66.75 66.75q7.5 7.75 7.5 18.5t-7.5 18.25l-3 3.25q-7.75 7.5-18.5 7.5t-18.5-7.5q-16.75-17-66.75-67l-66.75 67q-7.75 7.5-18.5 7.5t-18.25-7.5l-3-3.25q-7.75-7.5-7.75-18.25t7.75-18.5l84.75-84.75q-50.75-5.25-79.25-23.5-9.75-6.25-15.375-10.375t-10.625-9.25-6.625-9.625-1.125-10.125 3.75-12.375q2.5-5 7-8.75t10.5-5.5 14 0.5 16.25 8.75q1.25 1 3.75 2.75t10.75 6.125 17.25 7.625 23 6 28.25 2.75q22.75 0 43.5-6.375t30-12.625l9.5-6.25q8.25-6.5 16.25-8.75t14-0.5 10.5 5.5 7 8.75z"></path>
                        </svg>
                    </a>
                </div>
                <div class="socnets__item socnets__item_instagram">
                    <a target="_blank" class="socnets__icon" rel="nofollow" href="https://instagram.com/dmitriiarefev2335">
                        <svg viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
                            <path d="M14.154 16H1.846C.826 16 0 15.173 0 14.153V1.846C0 .826.826 0 1.846 0h12.308C15.174 0 16 .826 16 1.846v12.307c0 1.02-.826 1.847-1.846 1.847M8 4.923C6.3 4.923 4.923 6.3 4.923 8S6.3 11.077 8 11.077 11.077 9.7 11.077 8C11.077 6.3 9.7 4.923 8 4.923m6.154-2.462c0-.34-.275-.614-.616-.614h-1.846c-.34 0-.615.275-.615.615V4.31c0 .34.276.615.615.615h1.846c.34 0 .616-.276.616-.615V2.46zm0 4.31H12.76c.103.392.163.804.163 1.23 0 2.72-2.204 4.923-4.923 4.923-2.72 0-4.923-2.204-4.923-4.923 0-.426.06-.838.162-1.23H1.845v6.768c0 .34.275.615.616.615h11.076c.34 0 .616-.275.616-.615v-6.77z"></path>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
                <p></p>
                <div class="madeby">Разработка сайта - <a href="https://www.aisol.ru" target="_blank">АйтиРешения</a></div>
            </div>
        </footer>

       


    </div>


<?/*
    <div class="site__layout">

        <div id="container" class="site-container module main-page-module">

            <div class="row site-header__top-contacts hide-for-medium small-12">
                <div class="small-12 column">
                    <section id="topcontacts" data-block-id="3" data-ng-init="init(3)" data-ng-controller="TopContactsController" class="block-3 widget widget-block -nt-widget top-contacts" data-ng-class="{'on-view': !blocks[3].isEdit, 'edit-item': blocks[3].editItem}">
                        <div data-ng-show="!blocks[3].isEdit" class="content-block" id="topcontacts-show">
                            <div class="top-contacts -inline-group js-top-contacts">
                                <div class="medium-5 -inline-group top-contacts__phones">
                                    <div class="top-contacts__phones-list show-for-medium js-top-contacts__phones">
                                        <div class="desktop -inline-group"></div>
                                        <div class="mobile"></div>
                                    </div>
                                </div>

                                <div class="top-contacts__icon js-scroll-up hide-for-medium">
                                    <!--?xml version="1.0" encoding="utf-8"?-->

                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="768" height="768" viewBox="0 0 768 768">
                                        <path d="M384 256.5l192 192-45 45-147-147-147 147-45-45z"></path>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="js-header-constraint site-header-constraint" style="height: 179px;"></div>
            <header class="site-header js-sticky__header small-12 site-header_initial site-header_not-bottom site-header_pinned headroom--top" style="top: 0px;">
                <div class="site-header__content -relative">
                    <div class="row site-header__top-contacts show-for-medium">
                        <div class="small-12 column">
                            <section id="topcontacts" data-block-id="3" data-ng-init="init(3)" data-ng-controller="TopContactsController" class="block-3 widget widget-block -nt-widget top-contacts" data-ng-class="{'on-view': !blocks[3].isEdit, 'edit-item': blocks[3].editItem}">
                                <div data-ng-show="!blocks[3].isEdit" class="content-block" id="topcontacts-show">
                                    <div class="top-contacts -inline-group js-top-contacts">
                                        <div class="medium-5 -inline-group top-contacts__phones">
                                            <div class="top-contacts__phones-list show-for-medium js-top-contacts__phones">
                                                <div class="desktop -inline-group"></div>
                                                <div class="mobile"></div>
                                            </div>
                                        </div>


                                        <div class="top-contacts__icon js-scroll-up hide-for-medium">
                                            <!--?xml version="1.0" encoding="utf-8"?-->

                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="768" height="768" viewBox="0 0 768 768">
                                                <path d="M384 256.5l192 192-45 45-147-147-147 147-45-45z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="site-header__bottom-section js-combo-box" combo-box="menu">
                        <div class="row -inline-group site-header-bottom">
                            <div class="small-9 inline-column large-3">
                                <div class="-inline-group">
                                    <div class="small-2 medium-1 hide-for-large">
                                        <button class="site-header__menu-btn svg-icon js-combo-box__button_menu" data-toggle="site-menu">
                                            <!--?xml version="1.0" encoding="utf-8"?-->
                                            <!-- Generated by IcoMoon.io -->

                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="768" height="768" viewBox="0 0 768 768">
                                                <g id="icomoon-ignore"></g>
                                                <path d="M96 192h576v64.5h-576v-64.5zM96 415.5v-63h576v63h-576zM96 576v-64.5h576v64.5h-576z"></path>
                                            </svg>
                                        </button>
                                    </div><div class="small-10 medium-8 large-12">
                                        <section id="company-logo" class="block-1 widget-block widget-logo">
                                            <div id="site-logo" class="widget-logo__image">
                                                <link rel="image_src" href="/static/img/0000/0008/4562/84562820.i8hh3hzpmd.W215.png">
                                                <a href="/"><img src="<?= AppAsset::path('img/logo.png') ?>" alt="" style="max-width:215px;"></a>
                                            </div>
                                        </section>
                                    </div>

                                </div>
                            </div><div class="menu-cart-wrapper small-3 large-9 -inline-group_top">
                                <div class="js-combo-box__content_menu inline-column combo-box__content combo-box__content_invert large-12 inline-column">
                                    <div class="combo-box__header">
                                        <div class="combo-box__title">Меню</div>
                                        <div class="combo-box__close js-combo-box__close_menu svg-icon_smallx svg-icon">
                                            <!--?xml version="1.0" encoding="utf-8"?-->

                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="768" height="768" viewBox="0 0 768 768">
                                                <path d="M607.5 205.5l-178.5 178.5 178.5 178.5-45 45-178.5-178.5-178.5 178.5-45-45 178.5-178.5-178.5-178.5 45-45 178.5 178.5 178.5-178.5z"></path>
                                            </svg>
                                        </div>
                                    </div>

                                    <?= app\widgets\coolbaby\BottomMenu::widget() ?>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div></div>
            </header>
            <?= $content ?>
        </div>

        <footer  class="site-footer">
            <div class="row">
                <section class="small-12 column">
                    <div class="-inline-group">
                        <div class="small-12 large-8 text-center medium-text-left">
                            <div id="footer-text" class="footer-text client">
                                <div id="footertext1" class="footer-text__about">
                                    2007 - <?= date('Y') ?> © ДОМ МЕЧТЫ
                                </div>
                                <div id="footertext2" class="footer-text__desc">Ремонт в Костроме</div>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-4 site-footer_indent">

                        </div>
                    </div>
                </section>
            </div>
            <div id="temp-block"></div>

        </footer>
    </div>
*/?>
<?php $this->endBody() ?>
    </body>


        

</html>

<?php $this->endPage() ?>
