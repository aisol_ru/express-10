<?php

namespace app\commands;

use app\components\GmailApi;
use app\components\Mailer;
use app\components\SendFile;

class CronController extends \app\components\controller\Console
{

    public function actionBackup()
    {

        \app\components\Backup::run(true, true, true, true);

        return self::EXIT_CODE_NORMAL;
    }

  public function actionCreateFile()//скачиваем вложение с почты и формируем из него файл в папку web и Сохраняем данные из файла в БД
  {
    $api = new GmailApi();

//        if ($api->createFile()){
//            $api->saveFile();
//        }
  }

//    public function actionSaveFile()//Сохраняем данные из файла в БД
//    {
//        $api = new GmailApi();
//    }

  public function actionSendFile()//формируем данные из базы данных,делаем из них файл и отправляем куда указано в админке
  {
    $send = new SendFile();

    if ($send->createFile()) {
      $file = $send->file;
      $fileName = $send->fileName;
      Mailer::send(null, 'Новый файл', 'Пользователи изменившие данные', [$file => ['fileName' => $fileName]]);
      return true;
    }
  }
}
