<?php
/* @var $this app\components\View */

use app\forms\LoginForm;
use app\models\Parameter;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\assets\MainAsset;
use yii\helpers\Url;

MainAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <title><?= Html::encode($this->title) ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="yandex-verification" content="e5dac2ac2368f486"/>
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700&display=swap&subset=cyrillic"
              rel="stylesheet">
    </head>
    <?php $this->head() ?>

    <?php $this->beginBody() ?>
    <body>
    <div class="fade-wrap"></div>
    <div class="site-wrapper">
        <header class="header">
            <div class="header-top-row-wrap">
                <div class="header-top-row page-wrapper-wide">
                    <nav class="main-nav">
                        <ul>
                            <li class="main-logo">
                                <a href="/"><img src="<?= Parameter::getValue(2, true, true) ?>"
                                                 alt="<?= Yii::$app->name ?>"></a>
                            </li>

                            <li class="main-email">
                                <a href="mailto:info@10express.ru"><img
                                            src="<?= MainAsset::path('img/icon-mail-yellow.svg') ?>" alt="">info@10express.ru</a>
                            </li>

                            <li>
                                <a href="<?= (Yii::$app->urlManager->pageId == 1 ? '#company' : Url::to(['/#company'])); ?>">О
                                    компании</a>
                            </li>

                            <li>
                                <a href="<?= (Yii::$app->urlManager->pageId == 1 ? '#market' : Url::to(['/#market'])); ?>">Магазины</a>
                            </li>

                            <li>
                                <a href="<?= (Yii::$app->urlManager->pageId == 1 ? '#info' : Url::to(['/#info'])); ?>">Программа
                                    лояльности</a>
                            </li>

                            <li>
                                <a href="<?= (Yii::$app->urlManager->pageId == 1 ? '#tovar' : Url::to(['/#tovar'])); ?>">Товары
                                    10 баллов</a>
                            </li>
                            <li class="main-account">
                                <? if (Yii::$app->user->isGuest) { ?>
                                    <a href="<?= Url::to(['account/auth']) ?>" class="auth-link">Личный кабинет</a>
                                <? } else { ?>
                                    <a href="<?= Url::to(['account/auth']) ?>" class="auth-link">Личный кабинет</a>
                                    <?= Html::a('', 'logout', ['class' => 'link-logout']) ?>
                                <? } ?>
                            </li>
                            <li class="mobile-link-menu">
                                <a href="<?= Url::to(['account/auth']) ?>" class="auth-link">Личный кабинет</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="header-bot-row-wrap mobile-only"
                 style="<?= is_null($this->tree) === false && $this->tree->id == 3 ? 'display: none;' : '' ?>">
                <div class="header-bot-row page-wrapper">
                    <div class="logo-block">
                        <a href="/">
                            <img src="<?= Parameter::getValue(2, true, true) ?>" alt="<?= Yii::$app->name ?>">
                        </a>
                    </div>
                    <div class="header-cont-block">
                        <?php if (empty(Parameter::getValue(4, false, true)) === false) { ?>
                            <a href="mailto:<?= Parameter::getValue(4, false, true) ?>">
                                <div class="hcl-pic-wrap"><img src="<?= MainAsset::path('img/icon-mail.svg') ?>" alt="">
                                </div><?= Parameter::getValue(4, false, true) ?></a>
                        <?php } ?>
                        <?php if (empty(Parameter::getValue(5, false, true)) === false) { ?>
                            <a href="tel:+<?= preg_replace('/[\D]/', '', Parameter::getValue(5, false, true)) ?>">
                                <div class="hcl-pic-wrap"><img src="<?= MainAsset::path('img/icon-phone.svg') ?>"
                                                               alt=""></div><?= Parameter::getValue(5, false, true) ?>
                                <span>Служба качества</span></a>

                        <?php } ?>

                        <?php if (empty(Parameter::getValue(7, false, true)) === false) { ?>
                            <a href="tel:+<?= preg_replace('/[\D]/', '', Parameter::getValue(7, false, true)) ?>">
                                <div class="hcl-pic-wrap"><img src="<?= MainAsset::path('img/icon-phone.svg') ?>"
                                                               alt=""></div><?= Parameter::getValue(7, false, true) ?>
                                <span>Техническая поддержка</span></a>

                        <?php } ?>


                        <div class="burger-btn js-burger-btn">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div class="page-content">

            <?= $content ?>

        </div>
        <footer class="footer" id="footer">


            <? if (Yii::$app->user->isGuest && !empty(Yii::$app->params['auth_true']) === false) { ?>
                <div class="page-wrapper">
                    <div class="auth-block">
                        <?php $form = ActiveForm::begin([
                            'id' => 'login-form',
                            'action' => '/auth',
                            'options' => [],
                            'fieldConfig' => [
                                'template' => "<p><div>{label}</div>\n<div>{input}</div><div style=\"height: 10px\" class=\"error\">{error}</div></p>",
                                'labelOptions' => ['class' => 'control-label'],
                            ],
                        ]); ?>
                        <?= $form->field($this->params['data'], 'pin')->passwordInput(['class' => 'form-control', 'placeholder' => 'Пин код'])->label(false) ?>
                        <?= $form->field($this->params['data'], 'num_cart')->textInput(['class' => 'form-control', 'placeholder' => 'Номер карты'])->label(false) ?>
                        
                        <?= Html::input('submit', 'login-button', 'Войти') ?>
                        <?= $form->field($this->params['data'], 'accept_check')->checkbox()->label('Сохраняя данные, я соглашаюсь с <a href="https://yadi.sk/i/-h4DIaTBFFW5bg">политикой обработки персональных данных</a> и выражаю согласие с <a href="https://yadi.sk/i/GsXPsBsVXBgl6w">предоставлением персональных данных</a>') ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <? } ?>

            <div class="footer-wrapper page-wrapper">
                <div class="footer-menu desktop-only">
                    <nav>
                        <ul>
                            <li>
                                <a href="<?= (Yii::$app->urlManager->pageId == 1 ? '#company' : 'javascript:void(0)'); ?>">О
                                    компании</a>
                            </li>
                            <li>
                                <a href="<?= (Yii::$app->urlManager->pageId == 1 ? '#market' : 'javascript:void(0)'); ?>">Магазины</a>
                            </li>
                            <li>
                                <a href="<?= (Yii::$app->urlManager->pageId == 1 ? '#tovar' : 'javascript:void(0)'); ?>">Товары
                                    10 баллов</a>
                            </li>
                            <li class="main-account">
                                <a href="/account">Личный кабинет</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="copyright">2020 © «Десяточка экспресс» Все права защищены.</div>
                <div class="email"><a href="mailto:info@10express.ru"><img
                                src="<?= MainAsset::path('img/icon-mail-yellow.svg') ?>" alt="">info@10express.ru</a>
                </div>
            </div>
        </footer>

        <?= \app\widgets\popup\Consultation::widget() ?>

        <?= \app\widgets\popup\Callback::widget() ?>
    </div>
    <div class="cookie-panel cookie-panel-block js-cookie-panel-block" style="">
        <div class="cookie-panel-text">Сайт 10express.ru использует файлы cookies для обеспечения корректной работы. Продолжая использовать данный ресурс, вы автоматически соглашаетесь с использованием данной технологии.</div>
        <div class="cookie-panel-close-button js-cookie-panel-close">Закрыть</div>
    </div>
    </body>
    <?php $this->endBody() ?>
    </html>
<?php $this->endPage() ?>