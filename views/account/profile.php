<?php
/* @var $this app\components\View */

use app\assets\MainAsset;
use app\components\IcmsHelper;
use app\models\User;
use app\modules\icms\widgets\date_time_input\DateTimeInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="page-wrapper">
    <h1 class="align-left"><?= $this->h1 ?></h1>
    <div class="profile">
        <div class="inp-list">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'log-reg-block sky-form'],
                'fieldConfig' => [
                    'template' => "<p><div>{label}</div>\n<div>{input}</div><div class=\"error\">{error}</div></p>",
                    'labelOptions' => ['class' => 'control-label'],
                ],
            ]); ?>
            <div class="inp-wrapper-50">
                <div class="inp-holder">
                    <?= $form->field($user, 'name')->textInput(['class' => 'form-control', 'placeholder' => 'Имя', 'value' => $user->name])->label('Имя') ?>
                </div>
                <div class="inp-holder">
                    <?= $form->field($user, 'female')->textInput(['class' => 'form-control', 'placeholder' => 'Фамилия', 'value' => $user->female])->label('Фамилия') ?>
                </div>
            </div>
            <div class="inp-holder">
                <?= $form->field($user, 'patronymic')->textInput(['class' => 'form-control', 'placeholder' => 'Отчество', 'value' => $user->patronymic])->label('Отчество') ?>
            </div>

            <div class="inp-wrapper-30">
                <div class="inp-holder">
                    <?= $form->field($user, 'birth')->widget(DateTimeInput::class, ['clientOptions' => [
                        'timepicker' => false,
                        'autoclose' => true,
                        'weekStart' => 1, //неделя начинается с понедельника
                        'todayBtn' => true, //снизу кнопка "сегодня"
                    ]])->label('День Рождения') ?>
                </div>
                <div class="inp-holder">
                    <?= $form->field($user, 'telephone')->widget(\yii\widgets\MaskedInput::class, ['name' => 'telephone',
                        'mask' => '99999999999',])->textInput(['class' => 'form-control', 'placeholder' => 'Номер телефона', 'value' => $user->telephone])->label('Номер телефона') ?>
                </div>
                <div class="inp-holder">
                    <?= $form->field($user, 'email')->textInput(['class' => 'form-control', 'placeholder' => 'Электронна почта', 'value' => $user->email])->label('Email') ?>
                </div>
            </div>
            <div class="row">
                <?= $form->field($user, 'agreement1')->checkbox(['label' => 'Сохраняя данные, я соглашаюсь с <a href="/privacy_policy.pdf">политикой обработки персональных данных</a> и выражаю согласие с <a href="/agreement.pdf">предоставлением персональных данных</a>'])->label(false) ?>
            </div>
            <br>
            <div class="row">
                <?= $form->field($user, 'agreement2')->checkbox(['label' => 'Подтверждаю согласие на общедоступность вводимых данных'])->label(false) ?>
            </div>
            <br><br>

            <div class="row">
                <div class="profile-buttons">
                    <?= Html::submitButton('Сохранить данные', ['class' => 'btn btn-cool btn-md-sm']) ?>
                    <?= Html::a('Выйти', 'logout', ['class' => 'btn btn-cool btn-md-xs']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            <a target="_blank" href="<?=\yii\helpers\Url::to(['@web/upload/files/pravila_10express.pdf'])?>">Правила программы лояльности "Десяточка Экспресс"</a>
        </div>
        <div class="card-info">
            <img src="<?= MainAsset::path('img/card-demo.png') ?>">
            <div class="ball-tab">
                <table>
                    <tr>
                        <td>
                            <b>Сумма покупок в предыдущем месяце, рублей: </b>
                        </td>
                        <td>
                            <span class="amount"><?= (!empty($user->amount) ? $user->amount : 'Покупок нет') ?></span>
                        </td>
                    </tr>

                    <tr>
                        <td><b>Начисляется на каждые 10 руб с покупки в текущем месяце, баллов: </b>
                        </td>
                        <td><span class="amount"><?= (!empty($user->balls) ? $user->balls : 'Баллов нет') ?></span>
                        </td>
                    </tr>
                </table>
            </div>
            <p></p>
            <p></p>
            <? /* <p><b>Карта действует с </b><?//= (!empty($user->created_at) ? IcmsHelper::dateTimeFormat('d.m.Y', $user->created_at) : 'Даты нет' )?></p>*/ ?>

        </div>
    </div>
</div>
