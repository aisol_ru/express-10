<?php
/* @var $this app\components\View */

use app\assets\MainAsset;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>


    <div class="col-md-7 md-margin-bottom-50 log-reg-v3">
        <div class="rect-nohover rect-equal-height">
            <?= $this->tree->getContent() ?>
        </div>
    </div>

    <div class="profile">

        <h2>Вход в аккаунт</h2>

        <div class="inp-list">
        <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'log-reg-block sky-form'],
                'fieldConfig' => [
                    'template' => "<p><div>{label}</div>\n<div>{input}</div><div class=\"error\">{error}</div></p>",
                    'labelOptions' => ['class' => 'control-label'],
                ],
            ]); ?>
            <div class="form-group">
                <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control', 'placeholder' => 'Пин код'])->label(false) ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'login')->textInput(['class'=>'form-control', 'placeholder' => 'Логин'])->label(false) ?>
            </div>
            
            <div class="row">
                <?= Html::submitButton('Вход', ['class' => 'btn btn-cool btn-md-sm', 'name' => 'login-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
        </div>


            <div class="card-info">
                <img src="<?= MainAsset::path('img/card-demo.png') ?>">
                <p><b>Номер карты:</b>2234 4492 3304 349</p>
                <p><b>Пин-код:</b> 2234</p>
                <p><b>Карта действует с </b>12.12.2020</p>

            </div>

    </div>
