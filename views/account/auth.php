<?php
/* @var $this app\components\View */

use app\assets\MainAsset;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

Yii::$app->params['auth_true'] = true;
?>


<div class="col-md-7 md-margin-bottom-50 log-reg-v3">
    <div class="rect-nohover rect-equal-height">
        <!--        --><? //= $this->tree->getContent() ?>
    </div>
</div>
<h1><?= $this->h1 ?></h1>
<div class="page-wrapper">
    <div class="profile">
        <?= $this->tree->getContent() ?>
    </div>
    <div class="auth-block textpage-auth">
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'action' => '/auth',
            'options' => [],
            'fieldConfig' => [
                'template' => "<p><div>{label}</div>\n<div>{input}</div><div style=\"height: 10px\" class=\"error\">{error}</div></p>",
                'labelOptions' => ['class' => 'control-label'],
            ],
        ]); ?>
        
        <?= $form->field($this->params['data'], 'pin')->passwordInput(['class' => 'form-control', 'placeholder' => 'Пин код'])->label(false) ?>
        <?= $form->field($this->params['data'], 'num_cart')->textInput(['class' => 'form-control', 'placeholder' => 'Номер карты'])->label(false) ?>
        <?= Html::input('submit', 'login-button', 'Войти') ?>
        
        <?= $form->field($this->params['data'], 'accept_check')->checkbox()->label('Сохраняя данные, я соглашаюсь с <a href="privacy_policy.pdf">политикой обработки персональных данных</a> и выражаю согласие с <a href="agreement.pdf">предоставлением персональных данных</a>') ?>

        <?php ActiveForm::end(); ?>
    </div>


</div>