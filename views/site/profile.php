<?php
/* @var $this app\components\View */
use app\assets\MainAsset;
?>
<h1 class="align-left"><?= $this->h1 ?></h1>
<?= $this->tree->getContent() ?>

<div class="profile">
	<div class="inp-list">
		<div class="inp-holder"><label>ФИО</label><input type="text" value="Макаров Сергей павлович"></div>
		<div class="inp-holder"><label>Дата рождения</label><input type="text" value="12.08.1980"></div>
		<div class="inp-holder"><label>Телефон</label><input type="text" value="+7(910)222-33-22"></div>
		<div class="inp-holder"><label>E-mail</label><input type="text" value="test@mail.ru"></div>
		<div class="save-but">Отправить заявку на изменение данных</div>
	</div>

	<div class="card-info">
		<img src="<?= MainAsset::path('img/card-demo.png') ?>">
		<p><b>Номер карты:</b>2234 4492 3304 349</p>
		<p><b>Пин-код:</b> 2234</p>
		<p><b>Карта действует с </b>12.12.2020</p>

	</div>
</div>