<?php
/* @var $this \app\components\View */
/* @var $feedbackList \app\models\Feedback[] */
/* @var $model \app\models\Feedback */

use app\components\IcmsHelper;
use app\widgets\ActiveForm;
?>

<?php if (Yii::$app->getSession()->getFlash('FEEDBACK_SEND', false, true) === true) { ?>
    <div class="notice">
        Ваш отзыв отправлен. Он появится на сайте после проверки модератором
    </div>
<?php } ?>

<div class="feedback">

    <?php foreach ($feedbackList as $feedback) { ?>
        <div class="fb-elem">
            <div class="nm"><?= $feedback->name ?></div>

            <div class="dat"><?= IcmsHelper::dateTimeFormat('d f в H:i', $feedback->created_date) ?></div>

            <div class="descr"><?= nl2br($feedback->content) ?></div>

            <?php if (empty($feedback->image) === false) { ?>
                <div class="fb-image">
                    <img src="<?= $feedback->getResizeCache('image', 100, 100, 1) ?>">
                </div>
            <?php } ?>
        </div>
    <?php } ?>

</div>

<div style="display: flex;justify-content: center;">
    <div class="modal-win">
        <div class="modal-title">Добавить отзыв</div>

        <?php $form = ActiveForm::begin() ?>

        <?= $form->field($model, 'name')->textInput(['placeholder' => 'Как Вас зовут?'])->label(false) ?>

        <?= $form->field($model, 'content')->textarea(['placeholder' => 'Комментарий', 'cols' => 30, 'rows' => 10])->label(false) ?>

        <?= $form->field($model, 'image')->fileInput()->label('Прикрепить фото:<br><br>') ?>

        <div class="btn-row">
            <button type="submit" class="btn green-btn">Отправить</button>
        </div>
        <p class="mrules">Нажимая на кнопку «Отправить», вы соглашаетесь с условиями использования и обработки ваших персональных данных.</p>
        <?php $form::end() ?>

    </div>
</div>