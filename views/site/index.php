<?php
/* @var $this app\components\View */

/* @var $map \app\models\Map */

use app\assets\MainAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<? /*
<div class="video-wrapper desktop-only">

    <video class="bg-video" autoplay="" loop="" muted="">
        <source src="<?= MainAsset::path('video/main.mp4') ?>" type="video/mp4"></video>
        <div class="main-about-panel page-wrapper slider-srapper">
         <div class="main-pic-slider-unit-text-block">
            <p>Пройди обучение с нами</p>
            <span>и получи права всего за 23 000 рублей</span>
            <div class="btn-row">
                <a data-fancybox data-src="#modal-consultation" href="javascript:void(0);" class="btn">Отправить заявку</a>
            </div>
        </div>

        <div class="gosgibdd">
            <a href="https://www.gosuslugi.ru/pay" target="_blank"> <img src="<?= MainAsset::path('img/logo-gos.png') ?>" alt=""></a>
            <a href="https://xn--90adear.xn--p1ai/r/44" target="_blank"> <img src="<?= MainAsset::path('img/gibdd.png') ?>" alt=""> </a>
        </div>

        </div>

</div>




<section class="main-pic-slider-panel-section mobile-only">
    <div class="main-pic-slider-panel page-wrapper">



            <div class="main-pic-slider-unit-text-block">
                <p>Пройди обучение с нами</p>
                <span>и получи права всего за 23 000 рублей</span>
                <div class="btn-row">
                    <a data-fancybox data-src="#modal-consultation" href="javascript:void(0);" class="btn">Отправить заявку</a>
                </div>
            </div>


    </div>
    <div class="mouse-icon">
        <img src="<?= MainAsset::path('img/icon-mouse.svg') ?>" alt="">
    </div>
</section>


<section class="main-pic-slider-panel-section">
    <div class="main-pic-slider-panel page-wrapper">
        <div class="main-pic-slider-unit-text-block">
            <p>10 Экспресс</p>
            <span>Получить карту для участия в системе лояльности</span>
            <div class="btn-row">
                <a data-fancybox data-src="#modal-consultation" href="javascript:void(0);" class="btn">Отправить заявку</a>
            </div>
        </div>
        <div class="main-pic-graph-block-wrap">
            <div class="graph-block">
                <p class="mpg-title">Сравнительные кривые трат за 12 месяцев</p>
                <div class="mpg-pano">
                    <div class="graph-unit g-step-1 active">
                        <div class="graph-unit-text">
                            <span>8 400</span><br>
                            рублей
                        </div>
                    </div>
                    <div class="graph-unit g-step-2">
                        <div class="graph-unit-text">
                            <span>41 990</span><br>
                            рублей
                        </div>
                    </div>
                   
                </div>
                <div class="mpg-title-row">
                    <div class="mpg-title-unit active">С системой лояльности</div>
                    <div class="mpg-title-unit">Без системы<br> лояльности</div>
                </div>
            </div>
        </div>
    </div>
    <div class="mouse-icon">
        <img src="<?= MainAsset::path('img/icon-mouse.svg') ?> ">
    </div>
</section>

*/ ?>

<section class="main-pic-slider-panel-section">
    <div class="main-pic-slider-panel page-wrapper">
        <div class="main-text-woman">
            <div class="main-slider-text-clock">
                <div id="company" class="hd"><?= \app\models\Parameter::getValue(5); ?></div>

                <div class="slider-content">
                    <?= \app\models\Parameter::getValue(4); ?>
                </div>
                <div class="main-slider-img desktop-only">
                    <img src="<?= MainAsset::path('img/girl2.png') ?> ">
                </div>
            </div>

        </div>
</section>

<section class="main-block-about-section" id="market">
    <div class="main-about-panel page-wrapper">
        <div class="main-map-block">
            <div class="map-script-block">
                <?= \app\widgets\YandexMap::widget([
                    'map' => $map,
                    'marks' => $map->marks,
                ]); ?>
            </div>
            <pre style="display:none;">
                <?php var_dump($map->marks); ?>
            </pre>
            <div class="map-shop-list">
                <div class="hd">Магазины</div>
                <div>
<!--                    --><?//= \app\models\Parameter::getValue(7); ?>
                </div>
                <div>
                    <? foreach ($map->marks as $mark) {
                        //Временное условие. Убрать когда потребуются все адреса. Точки на карте в вызове виджета.
                        if ($mark->id == 6) { ?>
                            <a href="javascript:;"
                               onclick="map_w0.panTo([<?= $mark->coordinate_x ?>,<?= $mark->coordinate_y ?>], {flying: true});"><img
                                        src="<?= MainAsset::path('img/pin.svg') ?> "
                                        alt="<?= $mark->id ?>"><?= $mark->name ?></a>

                        <? }
                    } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="main-block-info-section" id="info">
    <div class="main-about-panel page-wrapper">
        <div class="main-info-block">
            <div class="hd">Программа лояльности</div>
            <div class="profile-promo">
                <div class="column">
                    <div class="hd-promo">Активируйте карту</div>
                    <p class="text-bold"><span class="red">1.</span><a href="/account"> Авторизуйтесь</a> в личном
                        кабинете</p>
                    <p class="text-bold"><span class="red">2. </span>Поздравляем, с этого момента вы можете получать
                        баллы</p>
                    <div class="wom-promo"><img src="<?= MainAsset::path('img/wom-promo.jpg') ?>" alt=""
                                                style="width:100%;"></div>
                </div>
                <div class="column">
                    <div class="hd-promo">Ваша накопительная карта</div>
                    <p>С этой картой вы можете совершать покупки всей семьей</p>
                    <p>Набор состоит из основной накопительной карты и 3-х семейных мини-карт, которые вы можете раздать
                        своим близким</p>
                    <div class="red-block-text-big">Совершайте покупки и копите баллы вместе</div>
                    <div class="card-promo"><img src="<?= MainAsset::path('img/card-promo.png') ?>" alt=""></div>
                    <div class="gray-inf">*Накапливать баллы можно как с основной карты, так и с семейных мини-карт
                    </div>
                </div>
                <div class="column">
                    <div class="hd-promo">Что делать с баллами?</div>
                    <p class="text-bold"><span class="red">1.</span> Копите</p>
                    <p>Сразу после регистрации и до конца календарного месяца Вы получите фиксированный курс начисления
                        баллов: за каждые 10 рублей = 1 балл</p>
                    <div class="red-block-text-big">Совершите покупки и получите больше баллов в следующем месяце!</div>
                    <p>Количество начисляемых баллов зависит от суммы покупок в предыдущем месяце</p>
                    <div class="promo-tab">
                        <div class="col">
                            <span>До 3000 руб</span>
                            <span>От 3000 до 7000 руб</span>
                            <span>Свыше 7000 руб</span>
                        </div>
                        <div class="vline"></div>
                        <div class="col">
                            <span class="red text-bold">1 балл</span>
                            <span class="red text-bold">2 балла</span>
                            <span class="red text-bold">3 балла</span>
                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="yellow-block-text-big">
                        ПЕРЕРАСЧЕТ количества начисляемых баллов за каждые 10 рублей покупки происходит ПЕРВОГО ЧИСЛА
                        каждого календарного месяца
                    </div>

                    <p class="text-bold"><span class="red">2.</span> Тратьте баллы</p>

                    <div class="red-block-text-big">
                        Баллы списываются по курсу:<br>
                        10 баллов = 1 рубль
                    </div>

                    <p>&nbsp;</p>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="main-check-block-section" id="tovar">
    <div class="page-wrapper">
        <div class="gray-block-text">
            <div></div>
            <div class="gray-text-wrapper">
                <div class="hd">
                    <?= \app\models\Parameter::getValue(6); ?>
                </div>
                <!--                <div class="anons">«10 баллов – баланс цены и качества!»</div>-->
                <div class="gray-text">
                    <?= \app\models\Parameter::getValue(3); ?>
                </div>
            </div>
        </div>
    </div>
</section>
