<?php


namespace app\components;


use app\models\User;
use Google_Client;
use Google_Service_Gmail;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;

class GmailApi extends ActiveRecord
{
    public $client;
    public $service;
    public $user = "me";
    public $raw = [];
    public $messageId;
    public $id;
    public $attachmentId;
    public $data;
    public $fileName;


    public function __construct()
   {
       parent::__construct();
       $this->client = new Google_Client();
       $this->service = new Google_Service_Gmail($this->getClient());
        if (empty($this->messageId = $this->getMessages())  === true){ //получаем все сообщения
        return false;
        }
       $this->id = trim($this->getMessage()->getId());//получаем ID сообщения
//     print_r($this->getMessage());exit();
//     print_r($this->getMessage()['payload']['parts'][1]['body']);exit();

       $this->attachmentId = $this->getMessage()['payload']['parts'][1]['body']['attachmentId'];
//      var_dump($this->attachmentId);exit();
       $this->data = $this->getAttachment();
       if ($this->fileName = $this->createFile()){
         if ($this->saveFile()){
//        return true;
           $this->deleteMessage();
         }
        }
//        $this->fileName = $this->createFile();
//       $this->saveFile();
//       $this->sendFile = $this->sendFile();
   }



  public function getClient()
  {

    $this->client->setApplicationName('Express-10');
    $this->client->setScopes(Google_Service_Gmail::MAIL_GOOGLE_COM);
    $alias = Yii::getAlias( '@webroot'. '/' .'credentials.json');
    $this->client->setAuthConfig($alias);
    $this->client->setAccessType('offline');
//    $this->client->setAccessToken('offline');
    $this->client->setPrompt('select_account consent');
//        $this->client->setDeveloperKey('AIzaSyDC-J7Zc3JFd1r3fJbJAAjtMNcDJsVDwYI');
//    $this->client->setApprovalPrompt('auto');
    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    $tokenAlias = Yii::getAlias( '@webroot'. '/' . $tokenPath);
    if (file_exists($tokenAlias)) {
      $accessToken = json_decode(file_get_contents($tokenAlias), true);
      $this->client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    // если access_token в файле token.json истек "expires_in":3599 - время действия
    if ($this->client->isAccessTokenExpired()) {

      $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());// получаем новый токен доступа с токеном обновления - "извлекается старый токен обновления"
      $newAccessToken = $this->client->getAccessToken();// записываем новый токен  [access_token] в виде массива
      $accessToken = array_merge($accessToken, $newAccessToken);
      file_put_contents($tokenAlias, json_encode($accessToken));

//            $this->client->getRefreshToken();
//           $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());// получаем новый токен доступа с токеном обновления - "извлекается старый токен обновления"
//
//            $newAccessToken = $this->client->getAccessToken();// записываем новый токен  [access_token] в виде массива
//            $accessToken = array_merge($accessToken, $newAccessToken);
//            file_put_contents($tokenAlias, json_encode($accessToken));
//
//        $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
//        $newAccessToken = $this->client->getAccessToken();
//        $accessToken = array_merge($accessToken, $newAccessToken);
//        file_put_contents('credentials.json', json_encode($accessToken));
////             Refresh the token if possible, else fetch a new one.
//        $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
//        file_put_contents('credentials.json', json_encode($this->client->getAccessToken()));
//        if ($this->client->getRefreshToken()) {
//            $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
//        } else {
//            // Request authorization from the user.
//            $authUrl = $this->client->createAuthUrl();
//            printf("Open the following link in your browser:\n%s\n", $authUrl);
//            print 'Enter verification code: ';
//            $authCode = trim(fgets(STDIN));
//
//            // Exchange authorization code for an access token.
//            $accessToken = $$this->lient->fetchAccessTokenWithAuthCode($authCode);
//            $this->client->setAccessToken($accessToken);
//
//            // Check to see if there was an error.
//            if (array_key_exists('error', $accessToken)) {
//                throw new Exception(join(', ', $accessToken));
//            }
//        }
////             Save the token to a file.
//        if (!file_exists(dirname($tokenAlias))) {
//            mkdir(dirname($tokenAlias), 0700, true);
//        }
//        file_put_contents($tokenAlias, json_encode($this->client->getAccessToken()));


    }
    return $this->client;
  }


//    public function getClient()
//    {
//    $client = new Google_Client();
//    $client->setApplicationName('Express-10');
//    $client->setScopes(Google_Service_Gmail::MAIL_GOOGLE_COM);
//      $alias = Yii::getAlias( '@webroot'. '/' .'credentials.json');
//    $client->setAuthConfig($alias);
//    $client->setAccessType('offline');
//    $client->setPrompt('select_account consent');
//
//      // Load previously authorized token from a file, if it exists.
//      // The file token.json stores the user's access and refresh tokens, and is
//      // created automatically when the authorization flow completes for the first
//      // time.
//    $tokenPath = 'token.json';
//    $tokenPath = Yii::getAlias( '@webroot'. '/' . $tokenPath);
//    if (file_exists($tokenPath)) {
//    $accessToken = json_decode(file_get_contents($tokenPath), true);
//    $client->setAccessToken($accessToken);
//    }
//
//    // If there is no previous token or it's expired.
//    if ($client->isAccessTokenExpired()) {
//      // Refresh the token if possible, else fetch a new one.
//      if ($client->getRefreshToken()) {
//        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
//      } else {
//        // Request authorization from the user.
//        $authUrl = $client->createAuthUrl();
//        printf("Open the following link in your browser:\n%s\n", $authUrl);
//        print 'Enter verification code: ';
//        $authCode = trim(fgets(STDIN));
//
//        // Exchange authorization code for an access token.
//        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
//        $client->setAccessToken($accessToken);
//
//        // Check to see if there was an error.
//        if (array_key_exists('error', $accessToken)) {
//          throw new Exception(join(', ', $accessToken));
//        }
//      }
//      // Save the token to a file.
//      if (!file_exists(dirname($tokenPath))) {
//        mkdir(dirname($tokenPath), 0700, true);
//      }
//      file_put_contents($tokenPath, json_encode($client->getAccessToken()));
//    }
//    return $client;
//    }

    public function getMessages() {
        $pageToken = NULL;
        $messages = array();
        $opt_param = array('q' => 'from:decards2@cvk-trade.ru');
        do {
            try {
                if ($pageToken) {
                    $opt_param['pageToken'] = $pageToken;
                }
                $messagesResponse = $this->service->users_messages->listUsersMessages($this->user, $opt_param);
                if ($messagesResponse->getMessages()) {
                    $messages = array_merge($messages, $messagesResponse->getMessages());
                    $pageToken = $messagesResponse->getNextPageToken();
//                    dd($messages);exit();
                }
//                $messagesResponse = $this->service->users_messages->listUsersMessages($this->user, $opt_param);
//                if ($messagesResponse->getMessages()) {
//                    $messages = array_merge($messages, $messagesResponse->getMessages());
//                    $pageToken = $messagesResponse->getNextPageToken();
//                    dd($messages);exit();
//                }
            } catch (\Exception $e) {
                print 'An error occurred: ' . $e->getMessage();
            }
        } while ($pageToken);

        return $messages;
    }

    public function getMessage() {
        try {
//            сделал переборку всех сообщений. получаетс что оно должно быть одно что бы перебралось
            foreach ($this->messageId as $messageId){
                $result = $messageId;
            }
            $message = $this->service->users_messages->get($this->user, $result->id);
            return $message;
        } catch (\Exception $e) {
            print 'An error occurred: ' . $e->getMessage();
        }
    }

    public function getAttachment(){

        $attachment = $this->service->users_messages_attachments->get($this->user, $this->id, $this->attachmentId);
        $data = $attachment;
        return $data->getData();
    }

    public function createFile(){

        $fileName =  IcmsHelper::dateTimeFormat('d.m.Y.s');
        $fileName = str_replace(' ', '', $fileName);
        $fileName = str_replace(':', '.', $fileName);

        $fileName = "create_" . $fileName . ".txt";
        $data = strtr($this->data, array('-' => '+', '_' => '/'));
        $alias = Yii::getAlias( '@import'. '/' . $fileName);
        $res = file_put_contents($alias, str_replace(' ', '', mb_convert_encoding(base64_decode($data), 'utf-8', 'cp1251' ) ));
        if ($res){
//            $this->fileName = $alias;
            return $alias;
        }

    }

    public function saveFile(){//Сохраняем файл в БД который получили ранее с почты

        $handle = fopen($this->fileName , 'r');

        $all = [];
        for ($line = 1; $row = fgetcsv($handle, 1000, ";"); $line++){
            $all[] = $row ;

            $user = User::find()->andWhere(['num_cart' => $row[0]])->one();
            if ($user){

              $user->scenario = 'updateCart';
              if (!empty($row[2])){
                $user->female = $row[2];
              }
              if (!empty($row[3])) {
                $user->name = $row[3];
              }
              if (!empty($row[4])) {
                $user->patronymic = $row[4];
              }
              if (!empty($row[5])) {
                $user->birth = $row[5];
              }
              if (!empty($row[6])) {
                $user->telephone = $row[6];
              }
              if (!empty($row[7])){
              $user->email = $row[7];
                }
              if (!empty($row[8])) {
                $user->amount = $row[8];
              }
              if (!empty($row[9])) {
                $user->balls = $row[9];
              }
              $user->save();
            } else {
              $user = new User();
              $user->scenario = 'newSave';
              $user->num_cart = $row[0];
              $user->pin = $row[1];
              $user->password_hash = Yii::$app->security->generatePasswordHash($row[1]);
              $user->female = $row[2];
              $user->name = $row[3];
              $user->patronymic = $row[4];
              $user->birth = $row[5];
              $user->telephone = $row[6];
              $user->email = $row[7];
              $user->amount = $row[8];
              $user->balls = $row[9];
              $user->save();
            }

        }
        return true;
    }

    public function deleteMessage() {
        try {
           $this->service->users_messages->delete($this->user, $this->id);

        } catch (\Exception $e) {
            print 'An error occurred: ' . $e->getMessage();
        }
    }












//    public function parser()
//    {
//        foreach ($this->all as $res) {
//            $user = new User();
//            $user->role = User::DEFAULT_ROLE;
//            $user->login = $res[1];
//            $user->password = $res[1];
//            $user->num_cart = $res[1];
//            $user->pin = $res[3];
//            $user->female = $res[2];
//            $user->name = $res[3];
//            $user->patronymic = $res[4];
//            $user->birth = $res[5];
//            $user->telephone = $res[6];
//            $user->email = $res[7];
//            $user->amount = $res[8];
//            $user->id_author = $res[0];
//            $user->balls = $res[9];
//            $user->save();
//        }
//    }



}