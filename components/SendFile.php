<?php


namespace app\components;


use app\components\IcmsHelper;
use app\models\User;
use Google_Client;
use Google_Service_Gmail;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;


class SendFile
{

    public $file;
    public $fileName;


    public function createFile(){ //Фомируем файл. в нем те пользователи которые обновляли данные со вчера по сегоднешнее время


        $fileName =  IcmsHelper::dateTimeFormat('d.m.Y.s');
        $fileName = str_replace(' ', '', $fileName);
        $fileName = str_replace(':', '.', $fileName);

        $fileName = "send_" . $fileName . ".txt";
        $alias = Yii::getAlias( '@import'. '/' . $fileName);
        $updateUser = User::find()
            ->andWhere(['=', 'update_user', User::UPDATE_USER])
            ->asArray()
            ->all();
        $data = "";
        if (count($updateUser) > 0){
          foreach ($updateUser as $value){
            $data .= $value['num_cart'].
              ';' . $value['pin'] .
              ';' . $value['female'] .
              ';' . $value['name'] .
              ';' . $value['patronymic'] .
              ';' . $value['birth'] .
              ';' . $value['telephone'] .
              ';' . $value['email'] .
              ';' . $value['amount'] .
              ';' . $value['balls'] .
              "\r\n";
          }
        } else {
//          $data .= 'Обновлений не было';
          return false;
        }



        if (file_put_contents($alias, mb_convert_encoding($data, 'cp1251', 'utf-8' ))) {
            $updateUser = User::find()
                ->andWhere(['update_user'=> User::UPDATE_USER])
                ->all();
            foreach ($updateUser as $value){
              $user = User::findIdentity($value->id);
              $user->scenario = "updateCart";
              $user->update_user = 0;
              $user->save();

            }
            $this->fileName = $fileName;
            $this->file = $alias;
            return true;
        }

    }


//    public function sendFile(){ //Формирование файла для отправки, тот кто изменил его данные и отправятся. добавить это в User afterSave
//        $fileName =  IcmsHelper::dateTimeFormat('d.m.Y.H:i:s') . ".txt" ;
//        $fileName = str_replace(' ', '', $fileName);
//        $fileName = str_replace(':', '.', $fileName);
//        $fileName = "send_" . $fileName;
//        $idUser = \Yii::$app->user->id;
//        $user = User::find()->andWhere(['id' => $idUser,])->asArray()->one();
//        $myFile = fopen($fileName, "w+");
//        $data = "";
//
////         foreach ($user as $value){
//        $data .= $user['num_cart'].
//            ';' . $user['pin'] .
//            ';' . $user['female'] .
//            ';' . $user['name'] .
//            ';' . $user['patronymic'] .
//            ';' . $user['birth'] .
//            ';' . $user['telephone'] .
//            ';' . $user['email'] .
//            ';' . $user['amount'] .
//            ';' . $user['id_author'] .
//            ';' . $user['balls'];
////                 "\r\n";
////         }
//        fwrite($myFile, mb_convert_encoding($data, 'cp1251', 'utf-8' ));
//        fclose($myFile);
//        $this->sendFile = $fileName;
//        return true;
//    }


}