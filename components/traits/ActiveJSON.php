<?php

namespace app\components\traits;


trait ActiveJSON
{

    /**
     * Получение значения из JSON-поля
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getJson($name, $default = null)
    {
        $jsonText = $this->json;
        $json = [];
        if (empty($jsonText) !== true) {
            $json = json_decode($jsonText, true);
        }

        return isset($json[$name]) === true ? $json[$name] : $default;
    }

    /**
     * Установка значения JSON-поля
     * @param string $name
     * @param mixed $value
     */
    public function setJson($name, $value)
    {
        $jsonText = $this->json;
        $json = [];
        if (empty($jsonText) !== true) {
            $json = json_decode($jsonText, true);
        }

        $json[$name] = $value;
        $this->json = json_encode($json);
    }

    /**
     * Удаление значения JSON-поля
     * @param string $name
     */
    public function unsetJson($name)
    {
        $jsonText = $this->json;
        $json = [];
        if (empty($jsonText) !== true) {
            $json = json_decode($jsonText, true);
        }
        if (isset($json[$name]) === false) {
            return;
        }
        unset($json[$name]);

        $this->json = json_encode($json);
    }

}
