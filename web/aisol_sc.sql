-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: aisol_sc
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `yii_smart_kassir_admin_menu`
--

DROP TABLE IF EXISTS `yii_smart_kassir_admin_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_admin_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) NOT NULL DEFAULT '0',
  `controller` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `isActive` int(1) NOT NULL DEFAULT '1',
  `in_button` int(1) NOT NULL DEFAULT '0',
  `parentName` varchar(255) NOT NULL DEFAULT '',
  `icon_class` varchar(255) NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL DEFAULT '0',
  `role` varchar(255) NOT NULL DEFAULT '',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_admin_menu`
--

LOCK TABLES `yii_smart_kassir_admin_menu` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_admin_menu` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_admin_menu` VALUES (1,'0','structure','structure/index','Структура',1,0,'','icon_nav_structure',0,'manager',0,1561034660),(2,'1','structure','structure/add','Страница',1,1,'id_page','icon_nav_structure',0,'developer',0,1561034665),(4,'3','sliders','sliders/slider_add','Слайдер',1,1,'','icon_nav_structure',0,'manager',0,1444130572),(5,'3','sliders','sliders/slide_add','Слайд',1,1,'slider_id','icon_nav_structure',0,'manager',0,1435918489),(6,'0','developer','developer/index','Разработчику',1,0,'','icon_nav_structure',300,'developer',0,1435842111),(7,'6','developer','developer/menu_add','Добавить пункт меню',1,1,'','icon_nav_structure',0,'developer',1433758009,1435842172),(8,'0','users','users/index','Пользователи',1,0,'','icon_nav_users',40,'admin',1433758048,1435840294),(9,'8','users','users/add','Новый пользователь',1,1,'','icon_nav_structure',0,'admin',1433758130,1435839233),(11,'10','contents','contents/categotie_add','Категорию',1,1,'','icon_nav_structure',0,'manager',1435230073,1435230073),(12,'10','contents','contents/content_add','Элемент',1,1,'content_categorie_id','icon_nav_structure',0,'manager',1435230118,1435926017),(14,'13','galleries','galleries/categorie_add','Галерею',1,1,'pid','icon_nav_structure',0,'manager',1436355209,1436355568),(15,'13','galleries','galleries/gallery_add','Фотографии',1,1,'gallery_categorie_id','icon_nav_structure',0,'manager',1436355209,1436355568),(17,'16','catalog','catalog/categorie_add','Категорию',1,1,'pid','icon_nav_structure',0,'manager',1436355209,1436355568),(18,'16','catalog','catalog/catalog_add','Товар',1,1,'catalog_categorie_id','icon_nav_structure',0,'manager',1436355209,1436355568),(20,'19','maps','maps/map_add','Добавить карту',1,1,'','icon_nav_structure',0,'manager',1443097878,1443189364),(21,'19','maps','maps/mark_add','Добавить метку',1,1,'map_id','icon_nav_structure',0,'manager',1443097975,1443099056),(22,'0','seo','seo/index','SEO',1,0,'','icon_nav_structure',99,'admin',1443422767,1443532071),(23,'0','parameters','parameters/index','Параметры',1,0,'','icon_nav_structure',100,'admin',1445431397,1449659070),(24,'23','parameters','parameters/add','Параметр',1,1,'','icon_nav_structure',0,'developer',1445433500,1449658583),(26,'25','feedbacks','feedbacks/add','Отзыв',1,1,'','icon_nav_structure',0,'manager',1446796097,1446796097),(27,'6','developer','developer/keys','Ключи',1,0,'','',0,'developer',1447658064,1447658064),(28,'6','developer','developer/index','Пункты меню',1,0,'','',0,'developer',1447658064,1447658064),(29,'6','developer','developer/key_add','Ключ',1,1,'','',0,'developer',1447658064,1447658064),(30,'16','catalog','catalog/props','Свойства',1,0,'','icon_nav_structure',100,'manager',1448350361,1457589454),(31,'16','catalog','catalog/prop_add','Свойство',1,1,'','icon_nav_structure',0,'developer',1448351611,1448363531),(32,'16','catalog','catalog/prop_groups','Группы свойств',1,0,'','icon_nav_structure',0,'developer',1448884178,1457589479),(33,'16','catalog','catalog/prop_group_add','Группу свойств',1,1,'','icon_nav_structure',0,'developer',1448884564,1457589483),(37,'36','orders','orders/deliverys','Доставка',1,0,'','icon_nav_structure',0,'manager',1453210480,1453210499),(38,'36','orders','orders/delivery_add','Способ доставки',1,1,'','icon_nav_structure',0,'manager',1453210531,1453210531),(39,'36','orders','orders/pays','Оплата',1,0,'','icon_nav_structure',0,'manager',1453210480,1453210499),(40,'36','orders','orders/pay_add','Способ оплаты',1,1,'','icon_nav_structure',0,'manager',1453210531,1453210531),(42,'41','banners','banners/group_add','Группу',1,1,'','icon_nav_structure',0,'manager',0,1492001576),(43,'41','banners','banners/banner_add','Баннер',1,1,'group_id','icon_nav_structure',0,'manager',0,1492001567),(44,'36','orders','orders/statuses','Статусы',1,0,'','icon_nav_structure',0,'manager',1453210480,1453210499),(45,'36','orders','orders/status_add','Статус',1,1,'','icon_nav_structure',0,'manager',1453210480,1453210499),(46,'6','developer','developer/modules','Модули',1,0,'','icon_nav_structure',0,'developer',1480578214,1480578214),(47,'6','developer','developer/module_add','Модуль',1,1,'','icon_nav_structure',0,'admin',1480578246,1480578246),(48,'6','developer','developer/dumps','Резервное копирование',1,0,'','icon_nav_structure',0,'developer',1488525191,1488525191);
/*!40000 ALTER TABLE `yii_smart_kassir_admin_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_auth_assignment`
--

DROP TABLE IF EXISTS `yii_smart_kassir_auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `yii_smart_kassir_auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `yii_smart_kassir_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_auth_assignment`
--

LOCK TABLES `yii_smart_kassir_auth_assignment` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_auth_assignment` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_auth_assignment` VALUES ('admin','2',1454589513),('developer','1',1454589283),('developer','3',1454588790),('developer','4',1491829141),('developer','5',1478166465);
/*!40000 ALTER TABLE `yii_smart_kassir_auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_auth_item`
--

DROP TABLE IF EXISTS `yii_smart_kassir_auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `type` (`type`),
  CONSTRAINT `yii_smart_kassir_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `yii_smart_kassir_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_auth_item`
--

LOCK TABLES `yii_smart_kassir_auth_item` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_auth_item` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_auth_item` VALUES ('admin',1,'Администратор',NULL,NULL,NULL,NULL),('developer',1,'Разработчик',NULL,NULL,NULL,NULL),('manager',1,'Менеджер',NULL,NULL,NULL,NULL),('user',1,'Пользователь',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `yii_smart_kassir_auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_auth_item_child`
--

DROP TABLE IF EXISTS `yii_smart_kassir_auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `yii_smart_kassir_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `yii_smart_kassir_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `yii_smart_kassir_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `yii_smart_kassir_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_auth_item_child`
--

LOCK TABLES `yii_smart_kassir_auth_item_child` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_auth_item_child` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_auth_item_child` VALUES ('developer','admin'),('admin','manager'),('admin','user');
/*!40000 ALTER TABLE `yii_smart_kassir_auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_auth_rule`
--

DROP TABLE IF EXISTS `yii_smart_kassir_auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_auth_rule`
--

LOCK TABLES `yii_smart_kassir_auth_rule` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `yii_smart_kassir_auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_keys`
--

DROP TABLE IF EXISTS `yii_smart_kassir_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_keys` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  `updated_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_keys`
--

LOCK TABLES `yii_smart_kassir_keys` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_keys` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_keys` VALUES (1,5,'yandex_metrika_client_id','',1447659000,1454580964),(2,5,'yandex_metrika_client_secret','',1447659404,1454580965),(3,5,'yandex_metrika_access_token','',1447659504,1454580965),(4,5,'yandex_metrika_counter_id','',1447659604,1454580949),(5,14,'Яндекс метрика','',1447669499,1493299949),(7,0,'email\'ы для рассылки','',1454067316,1454067316),(8,7,'jobabc@mail.ru','jobabc@mail.ru',1454067343,1454067343),(9,7,'blaga@aisol.ru','blaga@aisol.ru',1454067355,1454067358),(10,7,'suhenko@aisol.ru','suhenko@aisol.ru',1454067410,1454067410),(11,0,'emailFrom','noanswer@example.com',1454068061,1454069762),(12,0,'Коды метрик (для добавления через модуль SEO)','',1455969468,1493300642),(13,12,'yandex','',1455969518,1455973716),(14,0,'Сервисы Yandex','',1493299940,1493299940);
/*!40000 ALTER TABLE `yii_smart_kassir_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_log_console`
--

DROP TABLE IF EXISTS `yii_smart_kassir_log_console`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_log_console` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL DEFAULT '0' COMMENT 'От кого выполнен',
  `controller` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Контроллер',
  `action` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Действие',
  `command` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Команда',
  `start` datetime DEFAULT NULL COMMENT 'Запущен',
  `end` datetime DEFAULT NULL COMMENT 'Закончен',
  `exit_code` int(1) unsigned DEFAULT NULL COMMENT 'Код завершения',
  `created_at` int(11) unsigned DEFAULT NULL COMMENT 'Дата создания',
  `updated_at` int(11) unsigned DEFAULT NULL COMMENT 'Дата изменения',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_log_console`
--

LOCK TABLES `yii_smart_kassir_log_console` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_log_console` DISABLE KEYS */;
/*!40000 ALTER TABLE `yii_smart_kassir_log_console` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_map_marks`
--

DROP TABLE IF EXISTS `yii_smart_kassir_map_marks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_map_marks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `map_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `coordinate_x` double unsigned NOT NULL,
  `coordinate_y` double unsigned NOT NULL,
  `image_x` int(11) DEFAULT NULL,
  `image_y` int(11) DEFAULT NULL,
  `image_width` int(11) DEFAULT NULL,
  `image_height` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `color` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_map_marks`
--

LOCK TABLES `yii_smart_kassir_map_marks` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_map_marks` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_map_marks` VALUES (2,1,'testmark2','srgsnfhm4th','marks_2.png',57.765582155241,40.916577070561,24,70,48,76,1,1443174897,1488264597,'1'),(3,1,'testasefs','asge','',57.765294377326,40.918106741938,0,0,0,0,1,1481633660,1491920719,'');
/*!40000 ALTER TABLE `yii_smart_kassir_map_marks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_maps`
--

DROP TABLE IF EXISTS `yii_smart_kassir_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_maps` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `mark_count` int(10) unsigned NOT NULL,
  `center_x` double unsigned NOT NULL,
  `center_y` double unsigned NOT NULL,
  `zoom` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `mark_default_color` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_maps`
--

LOCK TABLES `yii_smart_kassir_maps` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_maps` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_maps` VALUES (1,'Карта','asegseg\r\nsagsar',2,57.765242774878,40.917602486643,17,1,1443170745,1491920740,'');
/*!40000 ALTER TABLE `yii_smart_kassir_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_migration`
--

DROP TABLE IF EXISTS `yii_smart_kassir_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_migration`
--

LOCK TABLES `yii_smart_kassir_migration` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_migration` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_migration` VALUES ('m000000_000000_base',1432281813),('m130524_201442_init',1432281818),('m190620_132055_user_request',1561036887);
/*!40000 ALTER TABLE `yii_smart_kassir_migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_modules`
--

DROP TABLE IF EXISTS `yii_smart_kassir_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_modules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `route` varchar(255) NOT NULL DEFAULT '',
  `tree_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_modules`
--

LOCK TABLES `yii_smart_kassir_modules` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `yii_smart_kassir_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_parameters`
--

DROP TABLE IF EXISTS `yii_smart_kassir_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_parameters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text,
  `type` int(1) unsigned NOT NULL DEFAULT '1',
  `created_at` int(10) unsigned NOT NULL,
  `updated_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_parameters`
--

LOCK TABLES `yii_smart_kassir_parameters` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_parameters` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_parameters` VALUES (1,'email\'ы для получения уведомлений','',5,1454072325,1488877216),(2,'Логотип - хедер','image_2.svg',3,1454575464,1561032881),(3,'Логотип - футер','image_3.svg',3,1561032873,1561032899),(4,'Email - хедер','info@smart-cashier.ru',1,1561033028,1561033035),(5,'Телефон - хедер','+7 (999) 456-12-34',1,1561033189,1561033191),(6,'Копирайт','<p>{{date::Y}} &copy; Смарт кассир. Все права защищены.</p>\r\n',2,1561033527,1561033782);
/*!40000 ALTER TABLE `yii_smart_kassir_parameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_parameters_values`
--

DROP TABLE IF EXISTS `yii_smart_kassir_parameters_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_parameters_values` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parameter_id` int(11) NOT NULL,
  `value` text,
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` int(10) unsigned NOT NULL,
  `updated_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_parameters_values`
--

LOCK TABLES `yii_smart_kassir_parameters_values` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_parameters_values` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_parameters_values` VALUES (1,1,'partner@smart-cashier.ru',0,1488877224,1561037784);
/*!40000 ALTER TABLE `yii_smart_kassir_parameters_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_tree`
--

DROP TABLE IF EXISTS `yii_smart_kassir_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_tree` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `main_id` int(11) NOT NULL DEFAULT '0',
  `pid` int(11) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `auto_url` int(1) NOT NULL DEFAULT '1',
  `name_menu` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `h1_seo` varchar(255) NOT NULL DEFAULT '',
  `title_seo` text NOT NULL,
  `description_seo` text NOT NULL,
  `keywords_seo` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `in_menu_bottom` int(11) NOT NULL,
  `in_menu` int(1) NOT NULL,
  `in_map` int(1) NOT NULL,
  `nofollow` int(1) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `is_safe` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_tree`
--

LOCK TABLES `yii_smart_kassir_tree` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_tree` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_tree` VALUES (1,0,0,0,'','/',0,'Главная','','Главная','','','','',0,0,1,0,2147483647,1561031763,-100,1,1);
/*!40000 ALTER TABLE `yii_smart_kassir_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_tree_gallery`
--

DROP TABLE IF EXISTS `yii_smart_kassir_tree_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_tree_gallery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tree_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_tree_gallery`
--

LOCK TABLES `yii_smart_kassir_tree_gallery` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_tree_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `yii_smart_kassir_tree_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_user`
--

DROP TABLE IF EXISTS `yii_smart_kassir_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `auth_key` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_user`
--

LOCK TABLES `yii_smart_kassir_user` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_user` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_user` VALUES (1,'aisol','aisol','KY1N0rvm3O95owsnzS-cGZh4uzq_VX-n','$2y$13$hr0X6fyCZmGyzH2hqmnKy.wgmeyCOlUxFHn50glK6ay49SxFBQi.K',NULL,'info@aisol.ru',10,1432285516,1454589283,NULL,NULL,NULL,NULL),(3,'cyxen','Сергей Сухенко','Edh1S2KVV09o5R22XQSjofLsd1iQL2Ch','$2y$13$GWPWNrJsklCxo6BXwFh1B.qg/8RZVDQSQUDKvZWVQnlxj2gi9G8Vq',NULL,'suhenko@aisol.ru',10,1432294682,1454588790,NULL,NULL,NULL,NULL),(4,'blag','Артем Благовещенский','','$2y$13$I23vSKPq.Edahvclq4lPoujgCnrGiNNUjCE0MuB1dsLK2QiAd3CaC',NULL,'blaga@aisol.ru',10,1432294682,1491829141,NULL,NULL,NULL,NULL),(5,'alex','Александр Павлов','','$2y$13$IRWLGFZHLX8Gavh11A3vyOypudtfopHq/syeYeU4PZd7/CALwP5Au',NULL,'pavlov@aisol.ru',10,1478166465,1478166465,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `yii_smart_kassir_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_user_log`
--

DROP TABLE IF EXISTS `yii_smart_kassir_user_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_user_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(30) NOT NULL,
  `created_at` int(11) unsigned NOT NULL,
  `updated_at` int(11) unsigned NOT NULL,
  `developer_only` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_user_log`
--

LOCK TABLES `yii_smart_kassir_user_log` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_user_log` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_user_log` VALUES (1,4,'Артем Благовещенский','127.0.0.1',1464173211,1464173211,1),(2,4,'Артем Благовещенский','127.0.0.1',1469085522,1469085522,1),(3,4,'Артем Благовещенский','::1',1478165773,1478165773,1),(4,4,'Артем Благовещенский','::1',1479803296,1479803296,1),(5,4,'Артем Благовещенский','::1',1480504456,1480504456,1),(6,4,'Артем Благовещенский','::1',1481633599,1481633599,1),(7,4,'Артем Благовещенский','::1',1486621735,1486621735,1),(8,4,'Артем Благовещенский','::1',1488524308,1488524308,1),(9,4,'Артем Благовещенский','127.0.0.1',1491997297,1491997297,1),(10,4,'Артем Благовещенский','127.0.0.1',1491997319,1491997319,1),(11,4,'Артем Благовещенский','127.0.0.1',1491997339,1491997339,1),(12,4,'Артем Благовещенский','127.0.0.1',1491997452,1491997452,1),(13,4,'Артем Благовещенский','127.0.0.1',1491997487,1491997487,1),(14,4,'Артем Благовещенский','127.0.0.1',1491997847,1491997847,1),(15,4,'Артем Благовещенский','127.0.0.1',1491998413,1491998413,1),(16,4,'Артем Благовещенский','127.0.0.1',1491998722,1491998722,1),(17,4,'Артем Благовещенский','127.0.0.1',1492583452,1492583452,1),(18,4,'Артем Благовещенский','127.0.0.1',1494396047,1494396047,1),(19,4,'Артем Благовещенский','127.0.0.1',1495615606,1495615606,1),(20,4,'Артем Благовещенский','127.0.0.1',1496649472,1496649472,1),(21,4,'Артем Благовещенский','127.0.0.1',1497438385,1497438385,1),(22,4,'Артем Благовещенский','127.0.0.1',1498542894,1498542894,1),(23,4,'Артем Благовещенский','127.0.0.1',1502263965,1502263965,1),(24,4,'Артем Благовещенский','127.0.0.1',1502793211,1502793211,1),(25,4,'Артем Благовещенский','127.0.0.1',1503660362,1503660362,1),(26,4,'Артем Благовещенский','127.0.0.1',1507098735,1507098735,1),(27,4,'Артем Благовещенский','127.0.0.1',1509005066,1509005066,1),(28,4,'Артем Благовещенский','127.0.0.1',1509006402,1509006402,1),(29,4,'Артем Благовещенский','127.0.0.1',1509540351,1509540351,1),(30,6,'new_test_user','127.0.0.1',1509541373,1509541373,0),(31,4,'Артем Благовещенский','127.0.0.1',1511956541,1511956541,1),(32,4,'Артем Благовещенский','127.0.0.1',1518425705,1518425705,1),(33,4,'Артем Благовещенский','134.19.146.182',1561097852,1561097852,1);
/*!40000 ALTER TABLE `yii_smart_kassir_user_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_smart_kassir_user_request`
--

DROP TABLE IF EXISTS `yii_smart_kassir_user_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_smart_kassir_user_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Тип',
  `is_sended` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Флаг отправки email''а',
  `json` text COMMENT 'Содержимое формы',
  `created_at` int(11) unsigned DEFAULT NULL COMMENT 'Дата создания',
  `updated_at` int(11) unsigned DEFAULT NULL COMMENT 'Дата изменения',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_smart_kassir_user_request`
--

LOCK TABLES `yii_smart_kassir_user_request` WRITE;
/*!40000 ALTER TABLE `yii_smart_kassir_user_request` DISABLE KEYS */;
INSERT INTO `yii_smart_kassir_user_request` VALUES (1,2,0,'{\"name\":\"Test\",\"phone\":\"23235\",\"message\":\"test comment<br />\\r\\ntst\"}',1561037518,1561037518);
/*!40000 ALTER TABLE `yii_smart_kassir_user_request` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-21  9:50:28
