<?php

namespace app\assets;

use Yii;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class MainAsset extends AssetBundle
{

    public $sourcePath = '@app/assets/sources/main';
    
    public $css = [
        'css/normalize.css',
        'plugins/slick/slick.css',
        'fonts/fonts.css',
        'css/main.css',
    ];
    public $js = [
        'plugins/slick/slick.js',
        'js/script.js',
    ];
    public $depends = [
        JqueryAsset::class,
        FancyBox3Asset::class,
        GrowlAsset::class,
    ];

    public static function path($relativePath = '')
    {
        $obj = new self();
        return Yii::$app->assetManager->getPublishedUrl($obj->sourcePath) . '/' . $relativePath;
    }

    public function init()
    {
        parent::init();

        if (YII_DEBUG && !Yii::$app->request->isPjax) {
            $this->publishOptions['forceCopy'] = true;
        }

    }

}
