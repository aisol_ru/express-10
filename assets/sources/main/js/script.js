$(document).ready(function(){

    $('.fancy').fancybox({
        // Options will go here
    });

    $('.main-nav a').on('click', function(){
        $('.header-top-row-wrap').addClass('hid');
        $('.burger-btn').removeClass('active');
    });

    $('.clients-carousel').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            }
          ]
    });


    $('.techsupp-good-carousel').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 360,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
    });

    $('.often-answer-unit > p').on('click', function(){
        $(this).toggleClass('active').closest('.often-answer-unit').find('.often-answer-unit-text-block').slideToggle();
    });


    $('.js-burger-btn').on('click', function(){
      $(this).toggleClass('active');
      $(this).closest('header').find('.header-top-row-wrap').slideToggle();
      $('.header-top-row-wrap').removeClass('hid');
      
    });
    


    function mobileOnlySlider() {
 
        $('.js-plans-list').slick({
            slideToShow:2,
            slidesToScroll: 1,
            dots: true,
            arrows:false,
            infinite:true
        });
    }

    if(window.innerWidth < 768) {
    mobileOnlySlider();
    }



    // $('.js-more-link').on('click', function(){
    //     $('this').toggleClass('active');
    //     $(this).closest('.more-info-wrap-panel').find('.more-info-panel').slideToggle();
    // });

    // $('body').on('click','.quant-plus',function(){
    //     var input_quantity=$(this).parent().find('input');
    //     var num=parseInt(input_quantity.val());
    //     num++;
    //     input_quantity.val(num);
    // });

    // $('body').on('click','.quant-minus',function(){
    //     var input_quantity=$(this).parent().find('input');
    //     var num=parseInt(input_quantity.val());
    //     num--;
    //     if (num==0){
    //         return false;
    //     }
    //     input_quantity.val(num);
    // });

    // $('.share-row > a').on('click', function(){
    //     $(this).closest('.share-row').find('.share-soc-h-block').addClass('active');
    // });


    // $('.js-about-unit-car-wrap').slick({
    //     infinite: true,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     arrows: true,
    //     dots: false
    // });


    // $('.cert-pic-car').slick({
    //     infinite: true,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     arrows: true,
    //     dots: false
    // });

    



    // $('.js-out-stock-roll-link').on('click', function(){
    //     $(this).closest('.cart-out-of-stock-panel').find('.cart-out-of-stock-cont').slideToggle();
    //     if($(this).hasClass('active')) {
    //         $(this).removeClass('active');
    //         $(this).html('Развернуть');
    //     }
    //     else {
    //         $(this).addClass('active');
    //         $(this).html('Свернуть');
    //     }
    // });

    let CookiePanel = function () {

        const COOKIE_NAME = 'cookie-panel-status';


        let block = $('.js-cookie-panel-block');
        let closeButton = block.find('.js-cookie-panel-close');

        return {

            /**
             * Получает значение из куки
             * @param name string название
             * @param defaultValue mixed занчение по умолчанию
             * @returns string|undefined
             */
            get: function(name, defaultValue) {
                let matches = document.cookie.match(new RegExp(
                    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                ));
                return matches ? decodeURIComponent(matches[1]) : defaultValue;
            },


            /**
             * Устанавливает значение куки (старое перезаписывается)
             * @param name string название
             * @param value string занчение
             * @param options object параметры (expires)
             */
            set: function(name, value, options) {
                options = options || {};

                let expires = options.expires;

                if (typeof expires == "number" && expires) {
                    let d = new Date();
                    d.setTime(d.getTime() + expires * 1000);
                    expires = options.expires = d;
                }
                if (expires && expires.toUTCString) {
                    options.expires = expires.toUTCString();
                }

                value = encodeURIComponent(value);

                let updatedCookie = name + "=" + value;

                for (let propName in options) {
                    updatedCookie += "; " + propName;
                    let propValue = options[propName];
                    if (propValue !== true) {
                        updatedCookie += "=" + propValue;
                    }
                }

                document.cookie = updatedCookie;
            },

            /**
             * Инициализация панели кук
             */
            init() {
                if (CookiePanel.isShow() !== true) {
                    this.close()
                    return false;
                }

                closeButton.on('click', this.close);
                block.css('display', '');

                return true;
            },

            /**
             * Закрытие панели
             */
            close() {
                CookiePanel.set(COOKIE_NAME, 'hide', {'max-age': 2592000}); // На месяц
                block.hide();
            },

            /**
             * Проверка необходимости отображения панели
             */
            isShow() {
                return CookiePanel.get(COOKIE_NAME, true) === true;
            }
        };
    }();
    CookiePanel.init();

});