<?php


namespace app\widgets;


class ActiveForm extends \yii\widgets\ActiveForm
{

    public $validateOnBlur = false;
    public $fieldConfig = [
        'options' => [
            'class' => 'input-row',
        ],
    ];

}