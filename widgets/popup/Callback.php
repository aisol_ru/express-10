<?php

namespace app\widgets\popup;

use yii\base\Widget;

class Callback extends Widget
{

    public function run()
    {

        return $this->render('callback', [
            'model' => new \app\forms\Callback(),
        ]);
    }

}