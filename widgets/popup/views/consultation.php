<?php
/* @var $this \app\components\View */
/* @var $model \app\forms\Consultation */

use app\widgets\ActiveForm;
use app\widgets\AjaxSubmitButton;
use yii\web\JsExpression;

?>

<div class="modal-win" id="modal-consultation" style="display:none;">
    <div class="modal-title">Отправить заявку</div>
    <?php $form = ActiveForm::begin() ?>

        <?= $form->field($model, 'name')->textInput(['placeholder' => 'Как к вам обращаться?'])->label(false) ?>

        <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон'])->label(false) ?>

        <?= $form->field($model, 'message')->textarea(['placeholder' => 'Комментарий', 'cols' => 30, 'rows' => 10])->label(false) ?>
        
        <?/*= $form->field($model, 'place')->dropDownList([
                                                            '0' => 'Проспект Мира, 67',
                                                            '1' => 'ул. Пушкина 36/2',
                                                           
                                                        ])->label('Адрес школы:<br><br>');

  //      textarea(['placeholder' => 'Комментарий', 'cols' => 30, 'rows' => 10])->label(false)*/ ?>

        <div class="btn-row">
            <?=
            AjaxSubmitButton::widget([
                'useWithActiveForm' => $form->getId(),
                'label' => 'Записаться',
                'options' => ['class' => 'btn green-btn'],
                'ajaxOptions' => [
                    'type' => 'POST',
                    'url' => '/ajax/default/consultation',
                    'dataType' => 'json',
                    'success' => new JsExpression("function(data){
                                        if (data.success) {
                                            $.growl({ title: 'Заказ консультации', message: 'Ваше сообщение получено.<br>Мы свяжемся с Вами в ближайшее время.', time: 5000});
                                            form.find('input[type=\"text\"]').val('');
                                            form.find('textarea').val('');
                                            form.yiiActiveForm('resetForm');
                                            $.fancybox.close();
                                        } else {
                                            if (data.error == 1) {
                                                $.growl.error({ title: 'Заказ консультации', message: 'Ошибка. Заполните все поля',time: 5000});
                                            }
                                            if (data.error == 2) {
                                                $.growl.error({ title: 'Заказ консультации', message: 'Ошибка. Попробуйте позже',time: 5000});
                                            }
                                        }
                                    }"),
                ],
            ])
            ?>
        </div>
        <p class="mrules">Нажимая на кнопку «Отправить», вы соглашаетесь с условиями использования и обработки ваших персональных данных.</p>
    <?php $form::end() ?>
</div>