<?php

namespace app\widgets\popup;

use yii\base\Widget;

class Consultation extends Widget
{

    public function run()
    {

        return $this->render('consultation', [
            'model' => new \app\forms\Consultation(),
        ]);
    }

}