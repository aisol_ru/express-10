<?php

namespace app\controllers;

use app\components\controller\Frontend;
use app\components\Mailer;
use app\components\SendFile;
use app\forms\LoginForm;
use app\models\Feedback;
use app\models\Map;
use yii\web\ErrorAction;

class SiteController extends Frontend
{

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'index';
        $map = Map::findOne(1);


        $model = new LoginForm();

        return $this->render('index', [
            'map' => $map,
            'model' =>$model,
        ]);
    }

    public function actionPage()
    {
        return $this->render('textpage');
    }

  public function actionTestFile()
  {

    $send = new SendFile();

    if ($send->createFile()) {
      $file = $send->file;
      $fileName = $send->fileName;
      Mailer::send(null, 'Новый файл', 'Пользователи изменившие данные', [$file => ['fileName' => $fileName]]);
      $result = 'Файл отправлен' ;
      return $this->render('test-file', ['result' => $result]);
    }
      $result = 'что то пошло не так' ;
      return $this->render('test-file', ['result' => $result]);
  }

    public function actionProfile()
    {
        return $this->render('profile');
    }

    public function actionFeedback()
    {
        $model = new Feedback();
        $model->created_date = date('Y-m-d H:i:s');

        if ($model->load(\Yii::$app->getRequest()->post()) === true && $model->validate() === true) {
            $model->status = Feedback::STATUS_HIDDEN;
            $model->save(false);
            $model->saveFiles();

            \Yii::$app->getSession()->setFlash('FEEDBACK_SEND', true);
            return $this->refresh();
        }

        $feedbackList = Feedback::find()
            ->andWhere(['status' => Feedback::STATUS_ACTIVE])
            ->orderBy(['created_date' => SORT_DESC])
            ->all();

        return $this->render('feedback', [
            'feedbackList' => $feedbackList,
            'model' => $model,
        ]);
    }

}
