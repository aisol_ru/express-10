<?php

namespace app\models\user;

use app\components\traits\ActiveJSON;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * @property integer $id
 * @property integer $type
 * @property integer $is_sended
 * @property string $json
 *
 * @property integer $created_at
 * @property integer $updated_at
 */
class Request extends ActiveRecord
{
    use ActiveJSON;

    const TYPE_CALLBACK = 1;
    const TYPE_CONSULTATION = 2;

    public static function tableName()
    {
        return '{{%user_request}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    static function add($type, $attributes = [], $isSended = false)
    {
        $new = new self();

        $new->type = $type;
        $new->json = Json::encode($attributes);
        $new->is_sended = $isSended === true ? 1 : 0;

        return $new->save();
    }

}
