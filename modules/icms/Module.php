<?php

namespace app\modules\icms;

use Yii;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'app\modules\icms\controllers';

    public function init()
    {
        parent::init();
        
        $fixWebRoot = Yii::getAlias('@webroot');
        $pos = strrpos($fixWebRoot, '/icms');
        $posWin = strrpos($fixWebRoot, '\icms');
        if($pos !== false)    {
            $fixWebRoot = substr_replace($fixWebRoot, '', $pos, 5);
        } elseif ($posWin !== false) {
            $fixWebRoot = substr_replace($fixWebRoot, '', $posWin, 5);
        }
        Yii::setAlias('@webroot', $fixWebRoot);
        
        Yii::setAlias('@web', '/');

        Yii::setAlias('@upload', '@webroot/upload/icms');
        Yii::setAlias('@upload/images', '@upload/images');
        Yii::setAlias('@upload/files', '@upload/files');

        Yii::setAlias('@upload/web', '@web/upload/icms');
        Yii::setAlias('@images', '@upload/web/images');
        Yii::setAlias('@files', '@upload/web/files');

        \Yii::setAlias('@image_cache', '@webroot/resize/cache');
        \Yii::setAlias('@image_cache/web', '@web/resize/cache');
        
        \Yii::setAlias('@icms', '@app/modules/icms');
        \Yii::setAlias('@icms/views', '@icms/views');
        \Yii::setAlias('@icms/layouts', '@icms/views/layouts');
        
        \Yii::setAlias('@icms/assets', '@icms/assets/src');

        $this->setViewPath('@icms/views');
    }

}
