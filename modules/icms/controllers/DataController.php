<?php

namespace app\modules\icms\controllers;

use Yii;
use app\models\User;
use yii\filters\AccessControl;
use app\modules\icms\widgets\GreenLine;

class DataController extends \app\components\controller\Backend
{

//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::class,
//                'only' => ['index', 'add', 'edit', 'edit_password'],
//                'rules' => [
//                    [
//                        'actions' => ['index', 'add', 'edit', 'edit_password'],
//                        'allow' => true,
//                        'roles' => ['admin'],
//                    ],
//                ],
//            ],
//        ];
//    }

    public function actionIndex()
    {

        $user = User::findOne(Yii::$app->user->id);
        Yii::$app->view->params['breadCrumbs']['crumbs'] = [
            ['url' => ['data/index'], 'title' => 'Мои данные'],
            ['url' => '', 'title' => 'Редактирование моих данных'],
        ];
//        if (is_null($user) === true) {
//            return $this->redirect(['users/add']);
//        }
        $user->scenario = 'csv';

        if ($user->load(Yii::$app->request->post()) && $user->save()) {
            GreenLine::show();
            return $this->refresh();
        }
        return $this->render('index', ['roles' => User::getRolesAsArray(), 'model' => $user]);
    }


}
