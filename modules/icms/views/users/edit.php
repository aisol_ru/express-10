<?php

use app\modules\icms\widgets\date_time_input\DateTimeInput;
use yii\helpers\Html;
use app\modules\icms\widgets\ActiveFormIcms;
use app\modules\icms\widgets\drop_down_list\DropDownList;
use app\modules\icms\widgets\Tabs;
?>
<div class="data">
    <?php
    $form = ActiveFormIcms::begin();
    ?>
    <?php
        $tabs = Tabs::begin([
            'tabNames' => ['Общая информация']
        ]);
        ?>

        <?php $tabs->beginTab() ?>
            <div class='col-70'>
                <?= $form->field($model, 'num_cart')->textInput()->label("Номер карты") ?>
                <?= $form->field($model, 'pin')->textInput()->label('Пин код') ?>
                <?= $form->field($model, 'pin_repeat')->input('password')->label('Повторите пароль') ?>
                <?= $form->field($model, 'female')->textInput()->label('Фамилия') ?>
                <?= $form->field($model, 'name')->textInput()->label('Имя') ?>
                <?= $form->field($model, 'patronymic')->textInput()->label('Отчество') ?>
                <?= $form->field($model, 'email')->textInput()->label('Е-mail') ?>
                <?= $form->field($model, 'telephone')->widget(\yii\widgets\MaskedInput::class,['name' => 'telephone',
                  'mask' => '99999999999',])->textInput()->label('Номер телефона') ?>
                <?php if(Yii::$app->user->id == $model->id) {
                echo $form->field($model, 'birth')->widget(DateTimeInput::class,['clientOptions' => [
                    'timepicker' => false,
                    'autoclose'=>true,
                    'weekStart'=>1, //неделя начинается с понедельника
                    'todayBtn'=>true, //снизу кнопка "сегодня"
                ]])->label('День Рождения') ;
                } ?>

                <div class="clear"></div>
                <div class='col-70'>
                    <div class="action_buttons">
                        <a class="back">Назад</a>
                        <?= Html::submitButton('Сохранить', ['class' => 'save', 'name' => 'save-button']) ?>
                    </div>
                </div>
            </div>
            <div class="col-25 float_r">
                <?php if(Yii::$app->user->id != $model->id) {
                    echo $form->field($model, 'role')->widget(DropDownList::class, ['items' => $roles, 'placeholder' => 'Выберите группу'])->label('Группа') ;
                } ?>
                <?= $form->field($model, 'status')->widget(DropDownList::class, ['items' => $model::getStatuses(), 'placeholder' => 'Выберите статус'])->label('Статус') ?>
            </div>
        <?php $tabs->endTab() ?>
        <?php $tabs::end() ?>
    <?php ActiveFormIcms::end(); ?>
</div>