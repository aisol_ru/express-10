<?php

use yii\helpers\Html;
?>
<div class='add'>
    <span>Добавить</span>
    <div class='subnav'>
        <ul>
            <?php
            foreach ($adminMenu as $menuElem) {
                if (empty($menuElem->parentName) === false && isset($params[$menuElem->parentName]) === true) {
                    $route = [$menuElem->route, $menuElem->parentName => $params[$menuElem->parentName]];
                } else {
                    $route = [$menuElem->route];
                }
                ?>
                <li><?= Html::a($menuElem->title, $route) ?></li>
            <?php } ?>
        </ul>
    </div>
</div>
