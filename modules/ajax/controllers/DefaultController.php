<?php

namespace app\modules\ajax\controllers;

use Yii;
use app\components\controller\Ajax;
use app\components\Mailer;
use app\forms\Callback;
use app\forms\Consultation;
use app\models\user\user;

class DefaultController extends Ajax
{

    public function actionCallback()
    {
        $form = new Callback();
        $result = ['success' => false];
        if ($form->load(Yii::$app->request->post()) === true && $form->validate() === true) {
            $result['success'] = true;
            $message = <<<HTML
<p><b>Имя: </b>{$form->name}</p>
<p><b>Телефон: </b>{$form->phone}</p>
HTML;

            $isMailSend = Mailer::send(null, 'Заказ обратного звонка', $message);
            if ($isMailSend === false) {
                $result['success'] = false;
                $result['error'] = 2;
            }

        } else {
            $result['error'] = 1;
        }

        return $result;
    }

    public function actionConsultation()
    {
        $form = new Consultation();
        $result = ['success' => false];
        if ($form->load(Yii::$app->request->post()) === true && $form->validate() === true) {
            $result['success'] = true;
            $message = <<<HTML
<p><b>Имя: </b>{$form->name}</p>
<p><b>Телефон: </b>{$form->phone}</p>
<p><b>Сообщение: </b><br>{$form->message}</p>
HTML;

            $isMailSend = Mailer::send(null, 'Заказ консультации', $message);
            if ($isMailSend === false) {
                $result['success'] = false;
                $result['error'] = 2;
            }

        } else {
            $result['error'] = 1;
        }

        return $result;
    }


}
